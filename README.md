# Overview

This project is to fill a gap in the Argo Tunnel project as stated here - https://developers.cloudflare.com/argo-tunnel/reference/sidecar/

> Cloudflare does not publish an official Docker image for cloudflared, but will release one in the near future. cloudflared is written in Go and can be built in a Docker container by generating a Dockerfile that defines the Go version, the working directory, and the steps to run and install the application.

This project houses a simple way to build an image for `cloudflared` to run as a sidecar, and publish to our private registry.  There are other projects out there and this is essentially based on those.

We will delete this once an official image is released.



